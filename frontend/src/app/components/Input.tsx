import React from 'react';
import { InputText } from 'primereact/inputtext';
import { setConstantValue } from 'typescript';


interface Param {
    value:string, 
    label:string
}

export default class extends React.Component<Param, Param>{

    constructor(prop:Param){
        super(prop);
        this.state = {
            value: prop.value,
            label: prop.label
        }
    }

    setValue(text:string){
        this.setState({value: text});
    }

    render(){
        return (
            <span className="p-float-label">
      <InputText id="in" 
        value={this.state.value} 
        onChange={(e) => this.setValue(e.target.value)} 
    />
      <label htmlFor="in">{this.state.label}</label>
      </span>
        );
    }
}