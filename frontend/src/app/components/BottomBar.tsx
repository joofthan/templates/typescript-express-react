import React, {useState} from 'react';
import { TabMenu } from 'primereact/tabmenu';

function BottomBar() {

  //const [activeIndex, setActiveIndex] = useState(3);

  const items = [
      {label: '', icon: 'pi pi-fw pi-home'},
      {label: '', icon: 'pi pi-fw pi-calendar'},
      {label: '', icon: 'pi pi-fw pi-pencil'},
      {label: '', icon: 'pi pi-fw pi-file'},
      {label: '', icon: 'pi pi-fw pi-cog'}
  ];

  return (
    <div>
      <div className="card">
                <TabMenu model={items} />
            </div>
    </div>
    
  );
}

export default BottomBar;
