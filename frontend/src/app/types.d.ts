interface Todo {
    text: string;
    complete: boolean;
  }

interface Prop {
    todo: Todo
}