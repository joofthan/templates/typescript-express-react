import React from 'react';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import BottomBar from './components/BottomBar';
import Input from './components/Input';
import 'primereact/resources/themes/md-light-indigo/theme.css';
import DataTableBasicExample from "./components/DataTableBasicExample";
import PersonService from "./service/PersonService";
import Person from "../generated/api/person";



let todo : Todo = {text:'', complete : false};

let persi:Person = {
    name: "props.name",
    alter: 12
};

export default class App extends React.Component<Person, Person> {

    constructor() {
        super(persi);
        this.state = persi;

    }

    componentDidMount() {
        //this.setState({ person: "true" });
        let personService = new PersonService();
        personService.getCurrent().then(e=>{
            this.setState({
                name:e.name,
                alter:e.alter,
                adresse:e.adresse.strasse
            })
        }).catch(e=>{
            console.log(e)
            alert("Error:"+ e)
        })
    }

  render() {

    return (
      <div>
        <h1>Hello {this.state.name} from {this.state.adresse}</h1>
        <Input value={todo.text} label='Hiss'/>
          <DataTableBasicExample/>
        <BottomBar/>
      </div>
      
    );
  }
}
