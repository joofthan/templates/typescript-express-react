import Person from "../../generated/api/person";
import Endpoint from "../../generated/api/Endpoint";


export default class PersonService{
    public async getCurrent():Promise<Person>{
        let person = await fetch(Endpoint.backendUrl).then(e=>e.json());
        return person;
    }
}