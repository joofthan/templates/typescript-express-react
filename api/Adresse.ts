export default class Adresse{
    public strasse:string = "";
    public hausnummer:number = 0;

    public getAll(){
        return this.strasse + " "+this.hausnummer;
    }
}