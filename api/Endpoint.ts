export default class Endpoint {
    private static frontendPort = 3000;
    private static backendPort = 8080;
    public static frontendUrl = "http://localhost:"+Endpoint.frontendPort;
    public static backendUrl = "http://localhost:"+Endpoint.backendPort;
}