import Endpoint from "./Endpoint";
import express from "express";
import * as Console from "console";
import Adresse from "./Adresse";

export default class Person{
    public name:string = "";
    public alter:number = 0;
    public adresse:Adresse = new Adresse;
}