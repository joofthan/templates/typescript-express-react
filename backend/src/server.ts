import cors from "cors";
import express from "express";
import Person from "../../api/Person";


let app = express();
app.use(cors());

app.get( "/", ( req, res ) => {
    let person = new Person();
    person.name ="Joni";
    person.alter = 12;
    person.adresse.strasse = "waldsee";
    person.adresse.hausnummer = 10;
    res.send( person );
} );

const port = 8080;
app.listen( port, () => {
    console.log( `server started at http://localhost:${ port }` );
} );